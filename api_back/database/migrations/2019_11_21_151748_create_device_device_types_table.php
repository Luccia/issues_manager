<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeviceDeviceTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('device_device_types', function (Blueprint $table) {
            $table->bigInteger('id_device')->unsigned();
            $table->bigInteger('id_device_type')->unsigned();

            $table->index(['id_device'], 'device_device_types_id_device_foreign');
            $table->index(['id_device_type'], 'device_device_types_id_device_type_foreign');


            $table->foreign('id_device')->references('id_device')->on('devices')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->foreign('id_device_type')->references('id_device_type')->on('device_types')
                    ->onDelete('cascade')
                    ->onUpdate('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('device_device_types', function(Blueprint $table) {
			$table->dropForeign('device_device_types_id_device_foreign');
            $table->dropForeign('device_device_types_id_device_type_foreign');

            $table->dropColumn('id_device');
            $table->dropColumn('id_device_type');
        });

        Schema::dropIfExists('device_device_types');
    }
}
