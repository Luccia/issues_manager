<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminIncidentStatusesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_incident_statuses', function (Blueprint $table) {
            $table->bigInteger('id_status')->unsigned()->index();
            $table->foreign('id_status')->references('id_status')->on('statuses')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->bigInteger('id_incident')->unsigned()->index();
            $table->foreign('id_incident')->references('id_incident')->on('incidents')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->bigInteger('id_admin')->unsigned()->index()->nullable();
            $table->foreign('id_admin')->references('id_admin')->on('admins')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_incident_statuses');
    }
}
