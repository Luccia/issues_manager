<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->bigIncrements('id_store');
            $table->bigInteger('id_partner')->unsigned()->index();
            $table->string('name_store',50);
            $table->string('address_1_store', 100);
            $table->string('address_2_store', 100);
            $table->string('phone_store',100);
            $table->string('email_store', 50);
            $table->string('postal_code_store', 20);
            $table->string('city_store', 50);
            $table->foreign('id_partner')->references('id_partner')->on('partners')
                ->onDelete('cascade')
                ->onUpdate('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
