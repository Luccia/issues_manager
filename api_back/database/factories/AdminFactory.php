<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Admin;
use Faker\Generator as Faker;
use Illuminate\Support\Facades\Hash;

$factory->define(Admin::class, function (Faker $faker) {

    return [
        'name_admin' => $faker->lastName(),
        'firstname_admin' => $faker->firstName(),
        'email_admin' => $faker->email(),
        'password_admin' => Hash::make('adminadmin')
    ];
});
