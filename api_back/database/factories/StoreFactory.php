<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Partner;
use App\Models\Store;
use Faker\Generator as Faker;

$factory->define(Store::class, function (Faker $faker) {

    $id_partner = Partner::all()->pluck('id_partner')->toArray();
    return [
        'id_partner' => $faker->randomElement($id_partner),
        'name_store' => $faker->company,
        'address_1_store' => $faker->address,
        'address_2_store'=> $faker->address,
        'phone_store' => $faker->phoneNumber,
        'email_store'=> $faker->companyEmail,
        'postal_code_store'=> $faker->postcode,
        'city_store'=> $faker->city,
    ];
});
