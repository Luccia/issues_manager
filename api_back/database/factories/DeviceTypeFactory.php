<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\DeviceType;
use Faker\Generator as Faker;

$factory->define(DeviceType::class, function (Faker $faker) {

    return [
       'model_device_type' => $faker->company(),
       'platform_device_type' => $faker->randomElement(['phone', 'tablet']),
       'brand_device_type' => $faker->text(10)
    ];
});
