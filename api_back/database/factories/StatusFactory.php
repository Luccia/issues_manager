<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Status;

$factory->define(Status::class, function ($name) {    
    return [
        'name_status' => $name,
    ];
});
