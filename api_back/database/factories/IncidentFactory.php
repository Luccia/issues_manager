<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Incident;
use App\Models\Device;
use App\Models\User;
use Faker\Generator as Faker;

$factory->define(Incident::class, function (Faker $faker) {
    
    $id_device = Device::all()->pluck('id_device')->toArray();
    $id_user = User::all()->pluck('id_user')->toArray();

    return [
        'id_device' => $faker->randomElement($id_device),
        'id_user' => $faker->randomElement($id_user),
        'title_incident' => $faker->text(15),
        'date_incident' => $faker->date,
        'description_incident'=> $faker->text(55),
    ];
});
