<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\User;
use App\Models\Store;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {

    $id_store = Store::all()->pluck('id_store')->toArray();
    
    return [
        'id_store' => $faker->randomElement($id_store),
        'name_user' => $faker->lastName,
        'firstname_user' => $faker->firstName,
        'email_user' => $faker->email,
        'password_user' => $faker->password,
    ];
});
