<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Store;
use App\Models\Device;
use Faker\Generator as Faker;

$factory->define(Device::class, function (Faker $faker) {
    
    $id_store = Store::all()->pluck('id_store')->toArray();

    return [
        'id_store' => $faker->randomElement($id_store),
        'serial_number_device' => $faker->creditCardNumber,
    ];
});
