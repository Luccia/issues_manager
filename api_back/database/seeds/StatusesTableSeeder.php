<?php

use App\Http\Resources\Incident;
use App\Models\Status;
use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $statuses = ['new', 'in_progress', 'done'];

        foreach ($statuses as $status) {
            factory(Status::class, 1)->create(['name_status' => $status]);
        }
    }
}
