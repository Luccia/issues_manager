<?php

use Illuminate\Database\Seeder;
use App\Models\Device;
use App\Models\DeviceType;

class DevicesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Device::class, 10)->create()->each(function ($device) {
            $random_device_type = DeviceType::all()->random()->id_device_type;

            $device->deviceTypes()->attach($random_device_type);
        });
    }
}
