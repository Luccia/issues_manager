<?php

use Illuminate\Database\Seeder;
use App\Models\DeviceType;

class DeviceTypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(DeviceType::class, 10)->create();
    }
}
