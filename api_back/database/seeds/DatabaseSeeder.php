<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call([
            PartnersTableSeeder::class,
            StoresTableSeeder::class,
            UsersTableSeeder::class,
            AdminsTableSeeder::class,
            DeviceTypesTableSeeder::class,
            DevicesTableSeeder::class,
            StatusesTableSeeder::class,
            IncidentsTableSeeder::class,
        ]);
    }
}
