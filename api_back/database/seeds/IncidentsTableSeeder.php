<?php

use App\Models\Admin;
use App\Models\Status;
use App\Models\Incident;
use Illuminate\Database\Seeder;

class IncidentsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(Incident::class, 10)->create()->each(function ($incident) {
            $incident->statuses()->attach(Status::all()->random()->id_status, ['id_admin' => Admin::all()->random()->id_admin]);
        });
    }
}
