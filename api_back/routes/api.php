<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//  $router->group(
//   ['middleware' => [App\Http\Middleware\CorsMiddleware::class]],
//     function () use ($router) {
Route::post('admin/login', 'SecurityController@adminLogin');
 //   });

$router->group(
    ['middleware' => [App\Http\Middleware\JwtMiddleware::class]],
    function () use ($router) {

    /*
    |--------------------------------------------------------------------------
    | Group Device_types routes
    | This group contain all base routes for device_type actions
    |--------------------------------------------------------------------------
    */

    // List all device types
    Route::get('/device_types/platform/{platform}', 'DeviceTypeController@getBrandDevice');

    // List all device types
    Route::get('/device_types/brand/{platform}/{brand}', 'DeviceTypeController@getModelDevice');

    // List all device types
    Route::get('/device_types/model/{model}', 'DeviceTypeController@getIdDevice');

    // List all device types
    Route::get('device_types', 'DeviceTypeController@index');

    // List single device type
    Route::get('device_types/{id}', 'DeviceTypeController@show');

    // create new device type
    Route::post('device_types', 'DeviceTypeController@store');

    //update device type
    Route::put('device_types', 'DeviceTypeController@store');

    //delete device type
    Route::delete('device_types/{id}', 'DeviceTypeController@destroy');

    /*
    |--------------------------------------------------------------------------
    | Group Partners devices
    | This group contain all base routes for devices actions
    |--------------------------------------------------------------------------
    */

    // list stores
    Route::get('devices', 'DeviceController@index');

    // list single store
    Route::get('devices/{id}', 'DeviceController@show');

    // stocke les stores
    Route::post('devices', 'DeviceController@store');

    // update les stores
    Route::put('devices', 'DeviceController@store');

    // update single store
    Route::delete('devices/{id}', 'DeviceController@destroy');

    /*
    |--------------------------------------------------------------------------
    | Group Admins routes
    | This group contain all base routes for admins actions
    |--------------------------------------------------------------------------
    */

    // List all admin
    Route::get('admins', 'AdminController@index');

    // List single admin
    Route::get('admins/{id}', 'AdminController@show');

    // create new admin
    Route::post('admins', 'AdminController@store');

    //update admin
    Route::put('admins', 'AdminController@store');

    //delete admin
    Route::delete('admins/{id}', 'AdminController@destroy');

    // get current profile
    Route::get('profile', 'ProfileController@index');

    /*
    |--------------------------------------------------------------------------
    | Group Partners routes
    | This group contain all base routes for partners actions
    |--------------------------------------------------------------------------
    */

    // list partners
    Route::get('partners', 'PartnerController@index');

    // list single partner
    Route::get('partners/{id}', 'PartnerController@show');

    // stocke les partners
    Route::post('partners', 'PartnerController@store');

    // update les partners
    Route::put('partners', 'PartnerController@store');

    // update single partner
    Route::delete('partners/{id}', 'PartnerController@destroy');

    /*
    |--------------------------------------------------------------------------
    | Group Store routes
    | This group contain all base routes for stores actions
    |--------------------------------------------------------------------------
    */

    // list stores
    Route::get('stores', 'StoreController@index')->name('stores');

    // list single store
    Route::get('stores/{id}', 'StoreController@show');

    // list single store
    Route::get('stores/{id}/device', 'StoreController@getDeviceFromStore');

    // stocke les stores
    Route::post('stores', 'StoreController@store');

    // update les stores
    Route::patch('stores', 'StoreController@store');

    // update single store
    Route::delete('stores/{id}', 'StoreController@destroy');

    /*
    |--------------------------------------------------------------------------
    | Group Status routes
    | This group contain all base routes for Status actions
    |--------------------------------------------------------------------------
    */
    // List all Status
    Route::get('statuses', 'StatusController@index');

    // List single Status
    Route::get('statuses/{id}', 'StatusController@show');

    // create Status
    Route::post('statuses', 'StatusController@store');

    //update Status
    Route::put('statuses', 'StatusController@store');

    //delete single Status
    Route::delete('statuses/{id}', 'StatusController@destroy');

    /*
    |--------------------------------------------------------------------------
    | Group Incident routes
    | This group contain all base routes for Incident actions
    |--------------------------------------------------------------------------
    */
    // List all Incident
    Route::get('incidents', 'IncidentController@index');

    // List single Incident
    Route::get('incidents/{id}', 'IncidentController@show');

    // create Incident
    Route::post('incidents', 'IncidentController@store');

    //update Incident
    Route::put('incidents', 'IncidentController@store');

    //delete single Incident
    Route::delete('incidents/{incident}', 'IncidentController@destroy');

    //update Incident status
    Route::put('incidents/{incident}/statuses', 'IncidentController@updateStatus');

 });
