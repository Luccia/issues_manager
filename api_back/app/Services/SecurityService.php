<?php

namespace App\Services;

use Firebase\JWT\JWT;
use App\Models\Admin;

class SecurityService{

    //get token and authenticate
    public static $connectedUser;
     /**
     * Create a new token.
     *
     * @param Admin $admin
     *
     * @return string
     */
    public function generateToken(Admin $admin)
    {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $admin->id_admin, // Subject of the token
            'iat' => time(), // Time when JWT was issued.
            'exp' => time() + 60 * 60 
        ];

        // As you can see we are passing `JWT_SECRET` as the second parameter that will
        // be used to decode the token in the future.
        return JWT::encode($payload, env('JWT_SECRET'));
    }

}

