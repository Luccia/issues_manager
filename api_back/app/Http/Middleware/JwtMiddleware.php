<?php

namespace App\Http\Middleware;

use Closure;
use Exception;
use App\Models\Admin;
use App\Services\SecurityService;
use Firebase\JWT\JWT;
use Firebase\JWT\ExpiredException;

class JwtMiddleware {

    public function handle($request, Closure $next, $guard = null)
    {
        $token = $request->bearerToken();
        if (!$token) {
            // Unauthorized response if token not there
            return response()->json([
                'error' => $token
            ], 401);
        }
        try {
            $credentials = JWT::decode($token, env('JWT_SECRET'), ['HS256']);
        } catch (ExpiredException $e) {
            return Response()->json('test jwt expired Exception', 401);
        } catch (Exception $e) {
            return Response()->json([
                $e->getMessage()
            ], 401);
        }
        $admin = Admin::find($credentials->sub);
        SecurityService::$connectedUser = $admin;
        return $next($request);
    }
}
