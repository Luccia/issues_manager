<?php

namespace App\Http\Controllers;

use App\Models\Status;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StatusRequest;
use App\Http\Resources\Status as StatusResource;


class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        // get statuss
        $status = Status::paginate(15);

        //retrun list of statuss as a resource
        return response()->json(StatusResource::collection($status), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  StatusRequest  $request
     * @return JsonResponse
     */
    public function store(StatusRequest $request): JsonResponse
    {
        $status = $request->isMethod('put') ? Status::findOrFail($request->id_status) : new Status;

        $status->name_status = $request->name_status;

        if ($status->save()) {
            return response()->json(new StatusResource($status), 200);
        }

        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified resource.
     * @param  int  $id_status
     * @return JsonResponse
     */
    public function show($id_status): JsonResponse
    {
        // Get single status
        $status = Status::findOrFail($id_status);

        // Return single status as a resource
        return response()->json(new StatusResource($status), 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id_status
     * @return JsonResponse
     */
    public function destroy($id_status): JsonResponse
    {
        // Delete status
        $status = Status::findOrFail($id_status);

        if ($status->delete()) {
            return response()->json(new StatusResource($status));
        }

        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }
}
