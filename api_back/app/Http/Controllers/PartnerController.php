<?php

namespace App\Http\Controllers;

use App\Http\Requests\PartnerRequest;
use App\Models\Partner;
use App\Http\Resources\Partner as PartnerResource;
use Illuminate\Http\JsonResponse;

class PartnerController extends Controller
{
    /**
     * Display a listing of partners.
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        // return collection of partners as a resource
        return response()->json(PartnerResource::collection(Partner::paginate(15)), 200);
    }

    /**
     * Store a newly created partner in storage.
     * @param PartnerRequest $request
     * @return JsonResponse
     */
    public function store(PartnerRequest $request): JsonResponse
    {
        // check requests
        $validated = $request->validated();
        // check method
        $partner = $request->isMethod('put') ? Partner::findOrFail($request->id_partner) : new Partner;

        $partner->fill($validated);
        // update or create partner
        if ($partner->save()) {
            return response()->json(new PartnerResource($partner), 200);
        }
        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified partner.
     * @param  int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        // return single partner
        return response()->json(new PartnerResource(Partner::findOrFail($id)), 200);
    }

    /**
     * Remove the specified partner from storage.
     * @param  int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        // find partner
        $partner = Partner::findOrFail($id);
        // delete partner
        if ($partner->delete()) {
            return  response()->json(new PartnerResource($partner), 200);
        }
        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }
}
