<?php

namespace App\Http\Controllers;

use App\Models\Device;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\DeviceRequest;
use App\Http\Requests\DeviceTypeRequest;
use App\Http\Resources\Device as DeviceResource;

class DeviceController extends Controller
{
    /**
     * Display a listing of the devices.
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        // return collection of devices as a resource
        return response()->json(DeviceResource::collection(Device::paginate(35)),200);
    }

    /**
     * Store a newly created device in storage.
     * @param DeviceRequest $requestDevice
     * @param DeviceTypeRequest $requestDeviceType    *
     * @return JsonResponse
     */
    public function store(DeviceRequest $requestDevice): JsonResponse
    {
        // check requests
        $validatedDevice = $requestDevice->validated();
        // Check if request send id or not
        $device = $requestDevice->isMethod('put') ? Device::findOrFail($requestDevice->id_device) : new Device;
        // fill device with validate device
        $device->fill($validatedDevice);
        // update or create device
        if($device->save()){
            $device->deviceTypes()->attach([$requestDevice->id_device_type]);
            return response()->json(new DeviceResource($device), 200);
        }
        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified device.
     * @param  int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        // return single devices
        return response()->json(new DeviceResource(Device::findOrFail($id)), 200);
    }

    /**
     * Remove the specified device from storage.
     * @param  int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        // find device
        $device = Device::findOrFail($id);
        // delete device
        if($device->delete()){
            return response()->json(new DeviceResource($device));
        }
        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }
}
