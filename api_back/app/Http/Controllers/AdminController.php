<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Hash;
use App\Http\Resources\Admin as AdminResource;

class AdminController extends Controller
{

    protected $guard = 'web';
    protected $securityService;

  /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Display a listing of the resource.
     * @return JsonResponse
     */
    public function index() : JsonResponse
    {
        // get admins
        $admin = Admin::paginate(15);

        //return list of admins as a resource
        return response()->json(AdminResource::collection($admin), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request  $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $admin = $request->isMethod('put') ? Admin::findOrFail($request->id_admin) : new Admin;

        $admin->name_admin = $request->name_admin;
        $admin->firstname_admin = $request->firstname_admin;
        $admin->email_admin = $request->email_admin;
        $admin->password_admin = Hash::make($request->password_admin);
        $admin->setRememberToken(Str::random(60));

        if($admin->save()) {
            return response()->json(new AdminResource($admin), 200);
        }

        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified resource.
     * @param  int  $id_admin
     * @return JsonResponse
     */
    public function show($id_admin): JsonResponse
    {
        // Get single admin
        $admin = Admin::findOrFail($id_admin);

        // Return single admin as a resource
        return response()->json(new AdminResource($admin), 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id_admin
     * @return JsonResponse
     */
    public function destroy($id_admin): JsonResponse
    {
        // Delete admin
        $admin = Admin::findOrFail($id_admin);

        if($admin->delete()) {
            return response()->json(new AdminResource($admin));
        }

        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }
}
