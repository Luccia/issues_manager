<?php

namespace App\Http\Controllers;

use Illuminate\Http\JsonResponse;
use App\Services\SecurityService;

class ProfileController extends Controller
{
    /**
     * Display a the connected admin.
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        //retrun connected admin datas
        return response()->json(SecurityService::$connectedUser, 200);
    }
}
