<?php

namespace App\Http\Controllers;

use App\Models\DeviceType;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\DeviceTypeRequest;
use App\Http\Resources\DeviceType as DeviceTypeResource;

class DeviceTypeController extends Controller
{

    /**
     * Display a listing of the resource.
     * @return JsonResponse
    */
    public function index(): JsonResponse
    {
        //return list of device types as a resource
        return response()->json(DeviceTypeResource::collection(DeviceType::paginate(15)), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  DeviceTypeRequest $request
     * @return JsonResponse
    */
    public function store(DeviceTypeRequest $request): JsonResponse
    {
        // check request validation
        $validated = $request->validated();
        // Check if request send id or not
        $device_type = $request->isMethod('put') ? DeviceType::findOrFail($request->id_device_type) : new DeviceType;

        $device_type->fill($validated);

        if ($device_type->save()) {
            return response()->json(new DeviceTypeResource($device_type), 200);
        }
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified resource.
     * @param int $id_device_type
     * @return JsonResponse
    */
    public function show(int $id_device_type): JsonResponse
    {
        // Get single device type
        $device_type = DeviceType::findOrFail($id_device_type);
        // Return single device type as a resource
        return response()->json(new DeviceTypeResource($device_type), 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id_device_type
     * @return JsonResponse
    */
    public function destroy(int $id_device_type): JsonResponse
    {
        // Delete single device type
        $device_type = DeviceType::findOrFail($id_device_type);

        if ($device_type->delete()) {
            return response()->json(new DeviceTypeResource($device_type), 200);
        }
        return response()->json(['error' => 'Bad Request'], 400);
    }
    
    /**
     * Get brand from this platform device_type
     * @param string $platform
     * @return void
    */
    public function getBrandDevice(string $platform)
    {
        $brands = DB::table('device_types')->where('platform_device_type', '=', $platform)->distinct()->get('brand_device_type');
        return response()->json($brands, 200);
    }

    /**
     * Get model from this brand device_type
     * @param string $platform
     * @param string $brand
     * @return void
    */
    public function getModelDevice(string $platform, string $brand)
    {
        $models = DB::table('device_types')->where('platform_device_type', $platform)->where('brand_device_type', $brand)->get('model_device_type');
        return response()->json($models, 200);
    }

    /**
     * Get id device_type from this model
     * @param string $model
     * @return void
    */
    public function getIdDevice(string $model)
    {
        $idDeviceType = DB::table('device_types')->where('model_device_type', '=', $model)->get('id_device_type');
        return response()->json($idDeviceType, 200);
    }
}
