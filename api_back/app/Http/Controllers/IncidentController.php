<?php

namespace App\Http\Controllers;

use App\Models\Incident;
use Illuminate\Http\JsonResponse;
use App\Http\Requests\StatusChangeRequest;
use App\Http\Requests\IncidentRequest;
use App\Http\Resources\Incident as IncidentResource;

class IncidentController extends Controller
{
    /**
     * Display a listing of the resource.
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        // get incidents
        $incident = Incident::paginate(15);

        //retrun list of incidents as a resource
        return response()->json(IncidentResource::collection($incident), 200);
    }

    /**
     * Store a newly created resource in storage.
     * @param  IncidentRequest  $request
     * @return JsonResponse
     */
    public function store(IncidentRequest $request): JsonResponse
    {
        $validated = $request->validated();

        $incident = $request->isMethod('put') ? Incident::findOrFail($request->id_incident) : new Incident;

        $incident->fill($validated);

        if ($incident->save()) {
            return  response()->json(new IncidentResource($incident), 200);
        }

        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified resource.
     * @param  int  $id_incident
     * @return JsonResponse
     */
    public function show($id_incident): JsonResponse
    {
        // Get single incident
        $incident = Incident::findOrFail($id_incident);

        // Return single incident as a resource
        return response()->json(new IncidentResource($incident), 200);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int  $id_incident
     * @return JsonResponse
     */
    public function destroy($id_incident): JsonResponse
    {
        // find id
        $incident = Incident::findOrFail($id_incident);

        // Delete incident
        if ($incident->delete()) {
            return response()->json(new IncidentResource($incident));
        }

        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Update  the specified resource.
     * @param  StatusChangeRequest  $request
     * @param  int  $id_incident
     * @return JsonResponse
     */
    public function updateStatus(StatusChangeRequest $request, Incident $incident): JsonResponse
    {
        // update single incident status
        $validated = $request->validated();

        $incident->statuses()->attach($validated['id_status'], ['id_admin' => 1]);

        if ($incident->save()) {
            return  response()->json(new IncidentResource($incident), 200);
        }

        // Return single incident as a resource
        return response()->json(new IncidentResource($incident), 200);
    }
}
