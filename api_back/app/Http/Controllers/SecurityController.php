<?php

namespace App\Http\Controllers;

use App\Models\Admin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Services\SecurityService;


class SecurityController extends Controller{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->securityService = new SecurityService();

    }
        
    /**
     * Undocumented function
     *
     * @param Request $request
     *
     */
    public function adminLogin(Request $request) {

        $credentials = $request->only('email_admin','password_admin');
        $auth = Auth::guard('web')->attempt($credentials);
        if($auth){
            $admin = Admin::where("email_admin","=", $credentials['email_admin'])->first();
            return response()->json([
                'token' =>  $this->securityService->generateToken($admin)
            ], 200);
        }
        else{
            return response()->json([
                'error' => 'Email does not exist.'
            ], 401);
        }
    }
}

