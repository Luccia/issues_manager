<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreRequest;
use App\Models\Store;
use App\Http\Resources\Store as StoreResource;
use Illuminate\Http\JsonResponse;

class StoreController extends Controller
{
    /**
     * Display a listing of the stores.
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        // return collection of stores as a resource
        return response()->json(StoreResource::collection(Store::paginate(15)), 200);
    }

    /**
     * Store a newly created store in storage.
     * @param StoreRequest $request
     * @return JsonResponse
     */
    public function store(StoreRequest $request): JsonResponse
    {
        // check requests
        $validated = $request->validated();

        // check method
        $store = $request->isMethod('patch') ? Store::findOrFail($request->id_store) : new Store;
        $store->fill($validated);
        // update or create store
        if ($store->save()) {
            return response()->json(new StoreResource($store), 200);
        }
        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }

    /**
     * Display the specified store.
     * @param  int $id
     * @return JsonResponse
     */
    public function show(int $id): JsonResponse
    {
        // Get single store
        return response()->json(new StoreResource(Store::findOrFail($id)), 200);
    }
    /**
     * Display the specified store.
     * @param  int $id
     * @return JsonResponse
     */
    public function getDeviceFromStore(int $id): JsonResponse
    {
        $store = Store::find($id);
        $devices = $store->devices;
        // Get single store
        return response()->json($devices);
    }

    /**
     * Remove the specified resource from storage.
     * @param  int $id
     * @return JsonResponse
     */
    public function destroy(int $id): JsonResponse
    {
        // find store
        $store = Store::findOrFail($id);
        // delete store
        if ($store->delete()) {
            return response()->json(new StoreResource($store), 200);
        }
        // return error
        return response()->json(['error' => 'Bad Request'], 400);
    }
}
