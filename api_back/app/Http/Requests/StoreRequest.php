<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     * @return array
     */
    public function rules()
    {
        return [
        'id_partner' => 'integer',
        'name_store' => 'required',
        'address_1_store' => 'required',
        'address_2_store' => '',
        'phone_store' => 'required',
        'email_store' => 'email|required',
        'postal_code_store' => 'integer|required',
        'city_store' => 'required'
        ];
    }
}
