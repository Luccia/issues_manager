<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class User extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_user' => $this->id_user,
            'id_store' => $this->id_store,
            'name_user' =>$this->name_user,
            'firstname_user'=>$this->firstname_user,
            'email_user'=>$this->email_user,
            'password_user'=>$this->password_user,
        ];
    }
}
