<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class DeviceType extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */

    public function toArray($request)
    {
        return [
            'id_device_type' => $this->id_device_type,
            'model_device_type'=> $this->model_device_type,
            'platform_device_type'=> $this->platform_device_type,
            'brand_device_type'=> $this->brand_device_type,
        ];
    }
}
