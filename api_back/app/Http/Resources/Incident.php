<?php

namespace App\Http\Resources;

use Carbon\Carbon;
use Illuminate\Http\Resources\Json\JsonResource;

class Incident extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    
    public function toArray($request)
    {
        return [
            'id_incident' => $this->id_incident,
            'title_incident'=> $this->title_incident,
            'date_incident'=> isset($this->date_incident) ? Carbon::createFromFormat('Y-m-d H:i:s', $this->date_incident)->format('Y-m-d H:i') : null,
            'description_incident'=> $this->description_incident,
            'statuses' => Status::collection($this->statuses),
            'admins' => Admin::collection($this->admins),
            'device' => new Device($this->device),
            'user' => new User($this->user),
        ];
    }
}
