<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Admin extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_admin' => $this->id_admin,
            'name_admin'=> $this->name_admin,
            'firstname_admin'=> $this->firstname_admin,
            'email_admin'=> $this->email_admin,
            'password_admin'=> $this->password_admin
        ];
    }
}
