<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Device extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return
        [
            'id_device' => $this->id_device,
            'serial_number_device' => $this->serial_number_device,
            'store' => new Store($this->store),
            'device_types' => DeviceType::collection($this->deviceTypes)
        ];
    }
}
