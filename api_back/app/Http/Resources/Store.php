<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Store extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id_store' => $this->id_store,
            'name_store' =>$this->name_store,
            'address_1_store'=>$this->address_1_store,
            'address_2_store'=>$this->address_2_store,
            'phone_store'=>$this->phone_store,
            'email_store'=>$this->email_store,
            'postal_code_store'=>$this->postal_code_store,
            'city_store'=>$this->city_store,
            'partner' => new Partner($this->partner)
        ];
    }
}
