<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class DeviceType
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property int $id_device_type
 * @property string $model_device_type
 * @property string $platform_device_type
 * @property string $brand_device_type
 *
 * @property Collection $devices
 * @property Collection $device_types
 *
 * @package App\Models
 */
class DeviceType extends Model
{
    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id_device_type';

    /**
     * attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id_device_type',
        'model_device_type',
        'platform_device_type',
        'brand_device_type'
    ];

    /**
     * Get the devices device type for the devices type.
     */
    public function devices()
    {
        return $this->belongsToMany(Device::class, 'device_device_types', 'id_device_type', 'id_device')->withTimestamps();
    }
}
