<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Device
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property int $id_device
 * @property string $serial_number_device
 *
 * @property Collection $devices
 * @property Collection $store
 * @property Collection $incidents
 * @property Collection $device_types
 *
 * @package App\Models
 */
class Device extends Model
{
    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id_device';

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id_store' => 'int',
    ];

    /**
     * attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id_store',
        'serial_number_device'
    ];

    /**
     * Get the store that owns the device.
     */
    public function store()
    {
        return $this->belongsTo(Store::class, 'id_store');
    }

    /**
     * Get the devices device type for the devices.
     */
    public function deviceTypes()
    {
        return $this->belongsToMany(DeviceType::class, 'device_device_types', 'id_device', 'id_device_type')->withTimestamps();
    }

    /**
     * Get the incidents for the devices.
     */
    public function incidents()
    {
        return $this->hasMany(Incident::class, 'id_device');
    }
}
