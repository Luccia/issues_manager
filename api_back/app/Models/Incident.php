<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Incident
 * 
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * @property Carbon $date_incident
 * 
 * @property int $id_incident
 * @property string $title_incident
 * @property string $description_incident
 * 
 * @property Collection $incidents
 * @property Collection $device
 * @property Collection $user
 * @property Collection $statuses
 * @property Collection $admins
 *
 * @package App\Models
 */
class Incident extends Model
{
    protected $primaryKey = 'id_incident';

    protected $fillable = [
        'title_incident',
        'date_incident', 
        'description_incident'
    ];

    protected $casts = [
        'id_user' => 'int',
        'id_device' => 'int',
    ];

    protected $dates = [
        'date_incident'
    ];

    public function admins()
    {
        return $this->belongsToMany(Admin::class, 'admin_incident_statuses', 'id_incident', 'id_admin')->withTimestamps();
    }

    public function statuses()
    {
        return $this->belongsToMany(Status::class, 'admin_incident_statuses', 'id_incident', 'id_status')->withTimestamps();
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'id_user');
    }

    public function device()
    {
        return $this->belongsTo(Device::class, 'id_device');
    }
}
