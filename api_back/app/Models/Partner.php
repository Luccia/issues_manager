<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Partner
 * 
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property Collection $stores
 * @property Collection $partners
 *
 * @package App\Models
 */
class Partner extends Model
{
    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id_partner';

    /**
     * attributes that are mass assignable
     * @var array
     */
    protected $fillable = ['name_partner'];

    /**
     * Get the stores for the partner.
     */
    public function stores()
    {
        return $this->hasMany(Store::class, 'id_partner');
    }
}
