<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Store
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property Collection $stores
 * @property Collection $partner
 * @property Collection $devices
 * @property Collection $users
 *
 *
 * @package App\Models
 */
class Store extends Model
{
    /**
     * The primary key associated with the table.
     * @var string
     */
    protected $primaryKey = 'id_store';

    /**
     * The attributes that should be cast to native types.
     * @var array
     */
    protected $casts = [
        'id_partner' => 'int',
    ];

    /**
     * attributes that are mass assignable
     * @var array
     */
    protected $fillable = [
        'id_partner' ,
        'name_store',
        'address_1_store',
        'address_2_store',
        'phone_store',
        'email_store',
        'postal_code_store',
        'city_store'
    ];

    /**
     * Get the partner that owns the store.
     */
    public function partner()
    {
        return $this->belongsTo(Partner::class,'id_partner');
    }

    /**
     * Get the store for the store.
     */
    public function devices()
    {
        return $this->hasMany(Device::class, 'id_store');
    }

    /**
     * Get the store for the users.
     */
    public function users()
    {
        return $this->hasMany(User::class, 'id_store');
    }
}
