<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class User
 * 
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property int $id_user
 * @property string $name_user
 * @property string $firstname_user
 * @property string $email_user
 * @property string $password_user
 * 
 * @property Collection $store
 * @property Collection $incidents
 * @property Collection $users
 * 
 * @package App\Models
 */
class User extends Model
{
    protected $primaryKey = 'id_user';

    protected $fillable = [
        'name_user', 
        'firstname_user', 
        'email_user', 
        'password_user',
    ];

    protected $casts = [
        'id_store' => 'int',
    ];

    public function incidents()
    {
        return $this->hasMany(Incident::class, 'id_user');
    }
    
    public function store()
    {
        return $this->belongsTo(Store::class, 'id_store');
    }
}
