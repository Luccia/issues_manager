<?php

namespace App\Models;

use Carbon\Carbon;

use \Illuminate\Contracts\Auth\Authenticatable;
//use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Admin
 *
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @property int $id_admin
 * @property string $name_admin
 * @property string $firstname_admin
 * @property string $email_admin
 * @property string $password_admin
 *
 * @property Collection $admins
 * @property Collection $statuses
 * @property Collection $incidents
 *
 * @package App\Models
 */
class Admin extends  Model implements Authenticatable
{
    use Notifiable;

    protected $table = 'admins';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name_admin',
        'firstname_admin',
        'email_admin',
        'password_admin'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password_admin', 'remember_token'
    ];


    protected $primaryKey = 'id_admin';

      /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function statuses()
    {
        return $this->belongsToMany(Status::class, 'admin_incident_statuses', 'id_admin', 'id_status')->withTimestamps();
    }

    public function incidents()
    {
        return $this->belongsToMany(Incident::class, 'admin_incident_statuses', 'id_admin', 'id_incident')->withTimestamps();
    }
      /**
     * Get the name of the unique identifier for the user.
     *
     * @return string
     */
    public function getAuthIdentifierName(){
        return $this->email_admin;
    }

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier(){
        return $this->email_admin;
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword(){
        return $this->password_admin;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken(){
        return " ";
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value){
        return " ";
    }
    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName(){
        return " ";
    }
}
