<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Collection;

/**
 * Class Status
 * @property Carbon $created_at
 * @property Carbon $updated_at
 * 
 * @property int $id_status
 * @property string $name_status
 * 
 * @property Collection $statuses
 * @property Collection $incidents
 * @property Collection $admins
 *
 * @package App\Models
 */
class Status extends Model
{
    protected $primaryKey = 'id_status';
        
    protected $fillable = [
        'name_status'
    ];

    public function admins()
    {
        return $this->belongsToMany(Admin::class, 'admin_incident_statuses', 'id_status', 'id_admin')->withTimestamps();
    }

    public function incidents()
    {
        return $this->belongsToMany(Incident::class, 'admin_incident_statuses', 'id_status', 'id_incident')->withTimestamps();
    }
}
