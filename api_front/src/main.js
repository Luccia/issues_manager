import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import vuetify from '@/plugins/vuetify'
import 'vuetify/dist/vuetify.min.css'

Vue.config.productionTip = false
require('../src/assets/css/style.css');

new Vue({
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')