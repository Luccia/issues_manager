import Vue from 'vue'
import VueRouter from 'vue-router'

import Home from '../views/Home'
import Stores from '../views/Stores'
import Devices from '../views/Devices'
import Incidents from '../views/Incidents'
import IncidentEdit from '../views/IncidentEdit'
import StoreShow from '../views/StoreShow'
import Partner from '../views/Partner'
import Profiles from '../views/Profiles'
import Admin from '../views/Admin'
import Login from '../views/Login'

Vue.use(VueRouter);

export default new VueRouter({
 
  mode: 'history',
  base: process.env.BASE_URL,

  routes : [
    {
    path: '/home',
    name: 'home',
    component: Home
    },
    {
      path: '/stores',
      name: 'stores',
      component: Stores
    },
    {
      path: '/store/:id',
      name: 'store_edit',
      props: true,
      component: StoreShow
    },
    {
      path: '/devices',
      name: 'devices',
      component: Devices
    },
    {
      path: '/incidents',
      name: 'incidents',
      component: Incidents
    },
    {
      path: '/incidents/:id',
      name: 'incident_edit',
      props: true,
      component: IncidentEdit
    },
    {
      path: '/profiles',
      name: 'profiles',
      component: Profiles
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/partners',
      name: 'partners',
      component: Partner
    },
    {
      path: '/admins',
      name: 'admins',
      component: Admin
    },
  ]
});

