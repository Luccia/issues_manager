import axios from 'axios';
const API_URL = 'http://localhost:8000';
export class APIService{

constructor(){
}


getStores() {
    const url = `${API_URL}/api/stores`;
    return axios.get(url).then(response => response.data);
}

getStore(pk) {
    const url = `${API_URL}/api/stores/${pk}`;
    return axios.get(url).then(response => response.data)
    .catch(function (error) {
        console.log(error);
    }).finally(() => {
        this.loading = false
    });
}

}