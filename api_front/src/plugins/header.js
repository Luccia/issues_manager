export const requestOptions = {
    get() {
      return {
        method: "GET",
        ...headers()
      };
    },
    post(body) {
      return {
        method: "POST",
        ...headers(),
        body: JSON.stringify(body),
        mode: "cors"
      };
    },
    patch(body) {
      return {
        method: "PATCH",
        ...headers(),
        body: JSON.stringify(body),
        mode: "cors"
      };
    },
    put(body) {
      return {
        method: "PUT",
        ...headers(),
        body: JSON.stringify(body),
        mode: "cors"
      };
    },
    delete() {
      return {
        method: "DELETE",
        ...headers(),
        mode: "cors"
      };
    }
  };
  
  export const headers = () => {
      const authHeader = localStorage.getItem("currentToken")
        ? { Authorization: "Bearer " + localStorage.getItem("currentToken") }
        : {};
      return {
        headers: {
          ...authHeader,
          "Content-Type": "application/json"
        }
      };
  }