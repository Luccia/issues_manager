import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    login: false
  },
  mutations: {
    login(state){
      state.login = true
    }
  },
  getters: {
    login: state => {
    return state.login
    }
  },
  actions: {
    async login({commit}){
      console.log('commit')
        commit('login');
    },
    // async logout({commit}){

    // },
  },
})
