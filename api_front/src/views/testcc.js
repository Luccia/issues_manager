<v-card>
<v-card-text>
  <v-container>
    <v-row align="center" justify="center">
      <v-col cols="12" sm="6" md="6">
        <v-select
          v-model="editedItem.name_store"
          label="Magasins"
          prepend-icon="mdi-store"
          :items="devices_data"
          item-text="store.name_store"
          item-value="store.id_store"
        ></v-select>
        <v-radio-group
          v-model="editedItem.platform_device_type"
          row
        >
          <v-icon large>mdi-cellphone-iphone</v-icon>

          <v-radio
            name="platform_device_type"
            :value="'phone'"
          ></v-radio>
          <v-icon large>mdi-tablet-ipad</v-icon>
          <v-radio
            cols="4"
            name="platform_device_type"
            :value="'tablet'"
          ></v-radio>
        </v-radio-group>
        <v-select
          v-model="editedItem.brand_device_type"
          label="Marque"
          prepend-icon="mdi-food-apple-outline"
          :items="devices_data"
          item-text="device_types[0].brand_device_type"
          item-value="device_types[0].brand_device_type"
        ></v-select>
        <v-select
          v-model="editedItem.model_device_type"
          label="Modèle"
          prepend-icon="mdi-cellphone-information"
          :items="devices_data"
          item-text="device_types[0].model_device_type"
          item-value="device_types[0].model_device_type"
        ></v-select>
        <v-text-field
          prepend-icon="mdi-numeric"
          v-model="editedItem.serial_number_device"
          label="Numéro de série"
        ></v-text-field>
        <v-card-actions>
          <v-btn
            color="lime darken-3"
            outlined
            text
            @click="close"
            >Cancel</v-btn
          >
          <v-btn color="lime darken-3" outlined text @click="save"
            >Save</v-btn
          >
        </v-card-actions>
      </v-col>
    </v-row>
  </v-container>
</v-card-text>
</v-card>